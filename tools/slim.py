from collections import deque
import ROOT
import ROOT.RDataFrame as RDF

# ROOT.EnableImplicitMT()
verbosity = ROOT.Experimental.RLogScopedVerbosity(ROOT.Detail.RDF.RDFLogChannel(), ROOT.Experimental.ELogLevel.kInfo)

import os
import argparse


def merge(sample_path, out_path, is_data: bool, version):
    sample_dict = {
        'data': 0,
        'HH_PowPythia': 1,
        'HH_VBF': 2,
        'WZ': 3,
        'VV': 4,
        'tZ': 5,
        'ttH': 6,
        'ttW': 7,
        'ttZ': 8,
        'VVV': 9,
        'VH': 10,
        'ttbar': 11,
        'Vgamma': 12,
        'VJets': 13,
    }

    sample_code_str = """
    if (Sample_Name == "{0}")
        return {1};
    """

    code = 'int get_code(TString Sample_Name) {\n' + \
           ''.join([sample_code_str.format(k, v) for k, v in sample_dict.items()]) + \
           '\n return -1; \n } \n'

    ROOT.gInterpreter.Declare(code)

    rdf = RDF("nominal", sample_path)
    report = rdf.Report()
    c = rdf.Count()

    rdf = rdf.Define("Sample_Code", "get_code(Sample_Name)")

    '''
    Apply MVA Output (not used for now)
    '''
    k_fold = 3

    idx_str = ""
    for i in range(k_fold):
        idx = deque(range(k_fold))
        idx.rotate(i)
        idx_dict = {
            'train': list(idx)[:-2],
            'test': list(idx)[-2],
            'apply': list(idx)[-1],
        }
        xml_dir = f"/lustre/collider/zhangyulei/ATLAS/hhml/Data_Slim/slim_xml.{version}/hh3l_BDTG_fold_{i}.xml"
        var_names = ROOT.TMVA.Experimental.RReader(xml_dir).GetVariableNames()
        n_vars = len(var_names)
        for var in var_names:
            rdf = rdf.Redefine(var, f'(float)({var})')

        ROOT.gInterpreter.ProcessLine(f'''
        TMVA::Experimental::RReader model_{i} ("{xml_dir}");
        auto computeModel_{i} = TMVA::Experimental::Compute<{n_vars}, float>(model_{i});
        ''')

        rdf = rdf.Redefine(
            f"bdtg_score_{i}",
            eval(f'ROOT.computeModel_{i}'),
            eval(f'ROOT.model_{i}.GetVariableNames()'),
        )
        idx_str += f'( (EvtNum % {k_fold} == {idx_dict["apply"]}) * bdtg_score_{i}.front() ) {"+" if i < k_fold - 1 else ""} '
    rdf = rdf.Redefine('bdtg_score', idx_str)

    snap_option = ROOT.RDF.RSnapshotOptions()
    snap_option.fOverwriteIfExists = True
    if not is_data:
        sample_rdf = {v: rdf.Filter(f'Sample_Code == {v}', k) for k, v in sample_dict.items() if k != 'data'}
        for k, v in sample_rdf.items():
            v.Snapshot("nominal", os.path.join(out_path, f'mc_{k}.root'), rdf.GetColumnNames(), snap_option)
    else:
        rdf.Snapshot("nominal", os.path.join(out_path, 'data.root'), rdf.GetColumnNames(), snap_option)
    print(c.GetValue())
    report.Print()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Analysis Framework for HH->Multilepton-> 3 leptons')
    parser.add_argument('mva_version', type=str, help="mva version")
    parser.add_argument('--data', action=argparse.BooleanOptionalAction, default=False, help='flag for DATA')
    parser.add_argument('--mc', action=argparse.BooleanOptionalAction, default=False, help='flag for MC')
    args = parser.parse_args()

    prefix = "/lustre/collider/zhangyulei/ATLAS/fw1/trilep/nominal/output"
    outfix = f"/lustre/collider/zhangyulei/ATLAS/hhml/Data_Slim/ntuples.{args.mva_version}"

    if not os.path.exists(outfix):
        os.makedirs(outfix)
    # data
    if args.data:
        sample_path = [f"{prefix}/data/{year}/*.root" for year in [2015, 2016, 2017, 2018]]
        merge(sample_path, outfix, True, version=args.mva_version)

    # MC
    if args.mc:
        sample_path = []
        for mc_dir in os.listdir(prefix):
            if mc_dir == 'data': continue
            sample_path += [os.path.join(prefix, mc_dir, dsid, "*.root") for dsid in
                            os.listdir(os.path.join(prefix, mc_dir))]
        merge(sample_path, outfix, False, version=args.mva_version)
