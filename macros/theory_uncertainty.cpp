std::vector<float> CalcTheoUncertainty(const ROOT::RVec<double> &mc_genWeights, float weight_mc, float sumWeight) {
//std::vector<float> CalcTheoUncertainty(float mc_genWeights, float weight_mc, float sumWeight) {
    float weight_Scale_UP = 1;
    float weight_Scale_DOWN = 1;
    float weight_Alphas_UP = 1;
    float weight_Alphas_DOWN = 1;
    float weight_PDF_UP = 1;
    float weight_PDF_DOWN = 1;
    float weight_FSR_UP = 1;
    float weight_FSR_DOWN = 1;
    float weight_ISR_UP = 1;
    float weight_ISR_DOWN = 1;

    for(int i = 0; i < (int)mc_map.size(); ++i) {
        int jx = std::get<0>(mc_map[i]);
        TString tx = std::get<1>(mc_map[i]);
        float fill_value = std::get<2>(mc_map[i]);

        if( tx != "") {
            if( (tx.Contains("muR=1") && tx.Contains(",muF=2")) || (tx.Contains("muR=01") && tx.Contains("muF=02")) ||
            (tx.Contains("renscfact=1") && tx.Contains("facscfact=2")) || tx.Contains("MUR1_MUF2") ) {
                weight_Scale_UP = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
            }
            else if( (tx.Contains("muR=1") && tx.Contains(",muF=05")) || (tx.Contains("muR=01") && tx.Contains("muF=05")) ||
            (tx.Contains("renscfact=1") && tx.Contains("facscfact=05")) || tx.Contains("MUR1_MUF05") ) {
                weight_Scale_DOWN = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
            }
            else if(tx.Contains("PDFset=266000") || tx.Contains("PDF270000")) {
                weight_Alphas_UP = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
            }
            else if(tx.Contains("PDFset=265000") || tx.Contains("PDF269000")) {
                weight_Alphas_DOWN = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
            }
          // else if(tx.Contains("isr:muRfac=1") && tx.Contains("fsr:muRfac=2")) {
          //   weight_FSR_UP = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
          // }
          // else if(tx.Contains("isr:muRfac=1") && tx.Contains("fsr:muRfac=05")) {
          //   weight_FSR_DOWN = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
          // }
            else if(tx.Contains("Var3cUp")) {
                weight_ISR_UP = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
            }
            else if(tx.Contains("Var3cDown")) {
                weight_ISR_DOWN = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
            }
            else if( tx.Contains("set=2600") || tx.Contains("set=260100") || tx.Contains("lhapdf=904") || tx.Contains("MUR1_MUF1_PDF261") || tx.Contains("MUR1_MUF1_PDF303")) {
                double var = mc_genWeights.at(jx) / weight_mc * sumWeight / fill_value;
                var = fabs(var-1) < 2 ? var : 1.0;
                weight_PDF_UP = weight_PDF_UP < var ? var : weight_PDF_UP;
                weight_PDF_DOWN = weight_PDF_DOWN > var ? var : weight_PDF_DOWN;
            }
        }
    }

    weight_Scale_UP    = fabs(weight_Scale_UP - 1.0)    < 2 ? weight_Scale_UP : 1.0;
    weight_Scale_DOWN  = fabs(weight_Scale_DOWN - 1.0)  < 2 ? weight_Scale_DOWN : 1.0;
    weight_Alphas_UP   = fabs(weight_Alphas_UP - 1.0)   < 2 ? weight_Alphas_UP : 1.0;
    weight_Alphas_DOWN = fabs(weight_Alphas_DOWN - 1.0) < 2 ? weight_Alphas_DOWN : 1.0;
    weight_FSR_UP      = fabs(weight_FSR_UP - 1)   < 2 ? weight_FSR_UP : 1.0;
    weight_FSR_DOWN    = fabs(weight_FSR_DOWN - 1) < 2 ? weight_FSR_DOWN : 1.0;
    weight_ISR_UP      = fabs(weight_ISR_UP - 1.0)   < 2 ? weight_ISR_UP : 1.0;
    weight_ISR_DOWN    = fabs(weight_ISR_DOWN - 1.0) < 2 ? weight_ISR_DOWN : 1.0;
    weight_PDF_UP      = fabs(weight_PDF_UP - 1)   < 2 ? weight_PDF_UP : 1.0;
    weight_PDF_DOWN    = fabs(weight_PDF_DOWN - 1) < 2 ? weight_PDF_DOWN : 1.0;


    std::vector<float> result = {
        weight_Scale_UP,
        weight_Scale_DOWN,
        weight_Alphas_UP,
        weight_Alphas_DOWN,
        weight_PDF_UP,
        weight_PDF_DOWN,
        weight_FSR_UP,
        weight_FSR_DOWN,
        weight_ISR_UP,
        weight_ISR_DOWN
    };

    return result;
}