# hhml_3lepton



## Getting started

Basic requirement:
- ROOT 6.26
- Python 3.9+


Suggested Environment:
```
lsetup "views LCG_102b_ATLAS_2 x86_64-centos7-gcc11-opt"
export PYTHONPATH=<PATH/TO/hhml_3lepton>:${PYTHONPATH}
```

Or simply:
```
source setup.sh
```

## How to run?

```
python3 runner.py -i INPUT [-o OUTPUT] [--data | --no-data] [--only_nominal | --no-only_nominal]
```

- `-i`: the path to the input root file
- `-o`: [optional] the output root file; if not set, will output to `<input>_output.root` 
- `--data`: flag for data
- `--only_nominal`: flag for running only nominal tree